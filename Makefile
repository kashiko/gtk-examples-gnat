all:
	make -C ascii
	make -C beowulf

clean:
	make -C ascii clean
	make -C beowulf clean
