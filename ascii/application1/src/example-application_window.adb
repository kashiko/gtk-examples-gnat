package body Example.Application_Window is

   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window
   is
      Self : constant Example_Application_Window :=
        new Example_Application_Window_Record;
   begin
      Initialize (Self, Application);
      return Self;
   end Example_Application_Window_New;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile)
   is
   begin
      null;
   end Open;

end Example.Application_Window;
