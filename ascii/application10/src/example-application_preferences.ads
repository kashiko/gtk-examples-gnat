with Example.Application_Window; use Example.Application_Window;
with Gtk.Dialog;                 use Gtk.Dialog;

private with Gtk.Font_Button;
private with Gtk.Combo_Box_Text;

package Example.Application_Preferences is

   type Example_Application_Preferences_Record is
     new Gtk_Dialog_Record with private;
   type Example_Application_Preferences is
     access all Example_Application_Preferences_Record;

   procedure Initialize
     (Self   : not null access Example_Application_Preferences_Record'Class;
      Window : not null access Example_Application_Window_Record'Class);
   function Example_Application_Preferences_New
     (Window : not null access Example_Application_Window_Record'Class)
      return Example_Application_Preferences;

private

   use Gtk.Font_Button;
   use Gtk.Combo_Box_Text;

   type Example_Application_Preferences_Record is new Gtk_Dialog_Record with
   record
      Font    : Gtk_Font_Button;
      Tab_Pos : Gtk_Combo_Box_Text;
   end record;

end Example.Application_Preferences;
