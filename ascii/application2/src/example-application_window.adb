with Example.Ext; use Example.Ext;
with Glib;        use Glib;
with Glib.Object; use Glib.Object;
with Gtk.Builder; use Gtk.Builder;
with Gtk.Widget;  use Gtk.Widget;

package body Example.Application_Window is

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class)
   is
   begin
      Gtk.Application_Window.Initialize (Self, Application);

      declare
         Builder : constant Gtk_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/window.ui");
         Window : constant Gtk_Application_Window :=
           Gtk_Application_Window (Get_Object (Builder, "main_window"));
         Content : constant Gtk_Widget :=
           Gtk_Widget (Get_Object (Builder, "content_box"));
         Width, Height : Gint;
      begin
         Set_Title (Self, Get_Title (Window));
         Get_Default_Size (Window, Width, Height);
         Set_Default_Size (Self, Width, Height);

         Ref (Content);
         Remove (Window, Content);
         Add (Self, Content);
         Unref (Content);

         Unref (Builder);
         Destroy (Window);
      end;
   end Initialize;

   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window
   is
      Self : constant Example_Application_Window :=
        new Example_Application_Window_Record;
   begin
      Example.Application_Window.Initialize (Self, Application);
      return Self;
   end Example_Application_Window_New;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile)
   is
   begin
      null;
   end Open;

end Example.Application_Window;
