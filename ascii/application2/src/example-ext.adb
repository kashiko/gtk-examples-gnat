with Glib.Error; use Glib.Error;

package body Example.Ext is

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtk_Builder
   is
      Builder     : constant Gtk_Builder := Gtk_Builder_New;
      Error       : aliased GError;
      Return_Code : Guint                :=
        Add_From_Resource (Builder, Resource_Path, Error'Access);
   begin
      if Error /= null then
         declare
            Message : constant String := Get_Message (Error);
         begin
            Error_Free (Error);
            raise Program_Error with Message;
         end;
      end if;
      return Builder;
   end Ext_Builder_New_From_Resource;

end Example.Ext;
