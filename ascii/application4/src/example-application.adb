with Ada.Text_IO;
with Example.Application_Window; use Example.Application_Window;
with Example.Ext;                use Example.Ext;
with Glib.Action_Map;            use Glib.Action_Map;
with Glib.Menu_Model;            use Glib.Menu_Model;
with Glib.Object;                use Glib.Object;
with Glib.Simple_Action;         use Glib.Simple_Action;
with Glib.Variant;               use Glib.Variant;
with GNAT.Strings;               use GNAT.Strings;
with Gtk.Application;            use Gtk.Application;
with Gtk.Builder;                use Gtk.Builder;
with Gtk.Widget;                 use Gtk.Widget;
with System;

package body Example.Application is

   procedure Preferences_Activate_Handler (Action : access Gsimple_Action;
      Parameter : Gvariant; Data : System.Address) with
      Convention => C;

   procedure Preferences_Activate_Handler (Action : access Gsimple_Action;
      Parameter : Gvariant; Data : System.Address)
   is
   begin
      null;
   end Preferences_Activate_Handler;

   procedure Quit_Activate_Handler (Action : access Gsimple_Action;
      Parameter : Gvariant; Data : System.Address) with
      Convention => C;

   procedure Quit_Activate_Handler (Action : access Gsimple_Action;
      Parameter                            : Gvariant; Data : System.Address)
   is
      Stub        : Example_Application_Record;
      Application : constant Example_Application :=
        Example_Application (Get_User_Data (Data, Stub));
   begin
      Quit (Application);
   end Quit_Activate_Handler;

   procedure Startup_Handler (Self : access Gapplication_Record'Class) is
   begin
      declare
         Preferences_Entry : constant GAction_Entry :=
           Build ("preferences", Preferences_Activate_Handler'Access);
         Quit_Entry : constant GAction_Entry :=
           Build ("quit", Quit_Activate_Handler'Access);
         App_Entries : constant GAction_Entry_Array :=
           (Preferences_Entry, Quit_Entry);
      begin
         Add_Action_Entries
           (Gtk_Application (Self), App_Entries, Convert (GObject (Self)));
      end;

      Add_Accelerator
        (Gtk_Application (Self), "<Ctrl>Q", "app.quit", Null_Gvariant);

      declare
         Builder : Gtk_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/app-menu.ui");
         App_Menu : Gmenu_Model :=
           Gmenu_Model (Get_Object (Builder, "appmenu"));
      begin
         Set_App_Menu (Gtk_Application (Self), App_Menu);
         Unref (Builder);
      end;
   end Startup_Handler;

   procedure Activate_Handler (Self : access Gapplication_Record'Class) is
      Window : Example_Application_Window :=
        Example_Application_Window_New (Example_Application (Self));
   begin
      Present (Window);
   end Activate_Handler;

   procedure Open_Handler (Application : Gtkada_Application;
      Files                            : GFile_Array)
   is
      use Widget_List;
      Windows : Glist;
      Window  : Example_Application_Window;
   begin
      Windows := Get_Windows (Application);
      if Length (Windows) > 0 then
         Window := Example_Application_Window (Get_Data (Windows));
      else
         Window :=
           Example_Application_Window_New (Example_Application (Application));
      end if;

      for File of Files loop
         Open (Window, File);
      end loop;

      Present (Window);
   end Open_Handler;

   procedure Initialize
     (Self           : not null access Example_Application_Record'Class;
      Application_Id : UTF8_String := ""; Flags : GApplication_Flags;
      Gtkada_Flags   : Gtkada_Application_Flags)
   is
   begin
      Gtkada.Application.Initialize
        (Self, Application_Id, Flags, Gtkada_Flags);

      On_Startup (Self, Startup_Handler'Access);
      On_Activate (Self, Activate_Handler'Access);
      On_Open (Self, Open_Handler'Access);
   end Initialize;

   function Example_Application_New (Application_Id : UTF8_String := "";
      Flags                                         : GApplication_Flags;
      Gtkada_Flags : Gtkada_Application_Flags) return Example_Application
   is
      Self : constant Example_Application := new Example_Application_Record;
   begin
      Example.Application.Initialize
        (Self, Application_Id, Flags, Gtkada_Flags);
      return Self;
   end Example_Application_New;

end Example.Application;
