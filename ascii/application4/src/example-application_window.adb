with Ada.Directories;       use Ada.Directories;
with Ada.IO_Exceptions;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Example.Ext;           use Example.Ext;
with Example.Streams;       use Example.Streams;
with Glib;                  use Glib;
with Glib.Object;           use Glib.Object;
with Gtk.Builder;           use Gtk.Builder;
with Gtk.Label;             use Gtk.Label;
with Gtk.Scrolled_Window;   use Gtk.Scrolled_Window;
with Gtk.Text_Buffer;       use Gtk.Text_Buffer;
with Gtk.Text_View;         use Gtk.Text_View;
with Gtk.Widget;            use Gtk.Widget;

package body Example.Application_Window is

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class)
   is
   begin
      Gtk.Application_Window.Initialize (Self, Application);

      declare
         Builder : constant Gtk_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/window.ui");
         Window : constant Gtk_Application_Window :=
           Gtk_Application_Window (Get_Object (Builder, "main_window"));
         Content : constant Gtk_Widget :=
           Gtk_Widget (Get_Object (Builder, "content_box"));
         Width, Height : Gint;
      begin
         Set_Title (Self, Get_Title (Window));
         Get_Default_Size (Window, Width, Height);
         Set_Default_Size (Self, Width, Height);

         Ref (Content);
         Remove (Window, Content);
         Add (Self, Content);
         Unref (Content);

         Self.Notebook := Gtk_Notebook (Get_Object (Builder, "notebook"));

         Unref (Builder);
         Destroy (Window);
      end;
   end Initialize;

   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window
   is
      Self : constant Example_Application_Window :=
        new Example_Application_Window_Record;
   begin
      Example.Application_Window.Initialize (Self, Application);
      return Self;
   end Example_Application_Window_New;

   Invalid_Path_Error : exception;

   function Read_All (Path : String) return Unbounded_String is
   begin
      if Path'Length < 1 or else not Exists (Path)
        or else Kind (Path) /= Ordinary_File then
         raise Invalid_Path_Error;
      end if;
      return R : Unbounded_String do
         declare
            File : Ada.Streams.Stream_IO.File_Type;
            S    : Stream_Access;
         begin
            Open (File, In_File, Path);
            S := Stream (File);
            declare
               Buffer_Size  : constant := 512;
               Input_Stream : aliased Buffered_Stream_Type (S, Buffer_Size);
            begin
               loop
                  declare
                     C : Character;
                  begin
                     Character'Read (Input_Stream'Access, C);
                     Append (R, C);
                  end;
               end loop;
            exception
               when Ada.IO_Exceptions.End_Error =>
                  null;
            end;
            Close (File);
         end;
      end return;
   end Read_All;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile)
   is
      Path     : constant UTF8_String         := Get_Path (File);
      Basename : constant UTF8_String         := Simple_Name (Path);
      Scrolled : constant Gtk_Scrolled_Window := Gtk_Scrolled_Window_New;
      View     : constant Gtk_Text_View       := Gtk_Text_View_New;
      Buffer   : constant Gtk_Text_Buffer     := Get_Buffer (View);
   begin
      Show (Scrolled);
      Set_Hexpand (Scrolled, True);
      Set_Vexpand (Scrolled, True);
      Set_Editable (View, False);
      Set_Cursor_Visible (View, False);
      Show (View);
      Add (Scrolled, View);
      Append_Page (Self.Notebook, Scrolled, Gtk_Label_New (Basename));

      declare
         Text : Unbounded_String;
      begin
         Text := Read_All (Path);
         Set_Text (Buffer, To_String (Text));
      exception
         when Invalid_Path_Error =>
            null;
      end;
   end Open;

end Example.Application_Window;
