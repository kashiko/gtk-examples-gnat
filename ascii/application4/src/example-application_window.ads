with Example.Application;    use Example.Application;
with Gtk.Application_Window; use Gtk.Application_Window;
with Gtkada.Application;     use Gtkada.Application;

private with Gtk.Notebook;

package Example.Application_Window is

   type Example_Application_Window_Record is
     new Gtk_Application_Window_Record with private;
   type Example_Application_Window is
     access all Example_Application_Window_Record;

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class);
   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile);

private

   use Gtk.Notebook;

   type Example_Application_Window_Record is new Gtk_Application_Window_Record with
   record
      Notebook : Gtk_Notebook;
   end record;

end Example.Application_Window;
