with Glib;        use Glib;
with Gtk.Builder; use Gtk.Builder;

package Example.Ext is

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtk_Builder;

end Example.Ext;
