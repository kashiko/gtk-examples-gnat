with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package Example.Config is

   type Example_Config is record
      Config_Directory : Unbounded_String;

      Font    : Unbounded_String := To_Unbounded_String ("Monospace 12");
      Tab_Pos : Unbounded_String := To_Unbounded_String ("top");
   end record;

   procedure Initialize (Config : in out Example_Config);
   procedure Read (Config : in out Example_Config);
   procedure Write (Config : Example_Config);

end Example.Config;
