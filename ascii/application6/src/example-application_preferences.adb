with Example.Application; use Example.Application;
with Example.Config;      use Example.Config;
with Example.Ext;         use Example.Ext;
with Glib.Application;    use Glib.Application;
with Glib.Object;         use Glib.Object;
with Glib.Properties;     use Glib.Properties;
with Gtk.Builder;         use Gtk.Builder;
with Gtk.Widget;          use Gtk.Widget;
with Gtk.Window;          use Gtk.Window;

package body Example.Application_Preferences is

   procedure Initialize
     (Self   : not null access Example_Application_Preferences_Record'Class;
      Window : not null access Example_Application_Window_Record'Class)
   is
   begin
      declare
         Builder : constant Gtk_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/prefs.ui");
         Dialog : constant Gtk_Dialog :=
           Gtk_Dialog (Get_Object (Builder, "preferences"));
         Box : constant Gtk_Widget :=
           Gtk_Widget (Get_Object (Builder, "vbox"));
      begin
         Gtk.Dialog.Initialize
           (Self, Get_Title (Dialog), Gtk_Window (Window),
            Modal or Ext_Use_Header_Bar);

         Set_Resizable (Self, Get_Resizable (Dialog));

         Ref (Box);
         Remove (Dialog, Box);
         Remove (Self, Get_Child (Self));
         Add (Self, Box);
         Unref (Box);

         Self.Font    := Gtk_Font_Button (Get_Object (Builder, "font"));
         Self.Tab_Pos := Gtk_Combo_Box_Text (Get_Object (Builder, "tab-pos"));

         Unref (Builder);
         Destroy (Dialog);

         Ext_Bind_Property
           (Example_Application (Get_Default).Config, "font", Self.Font,
            "font", Ext_Binding_Bidirectional);
         Notify (Example_Application (Get_Default).Config, "font");
         Ext_Bind_Property
           (Example_Application (Get_Default).Config, "tab-pos", Self.Tab_Pos,
            "active-id", Ext_Binding_Bidirectional);
         Notify (Example_Application (Get_Default).Config, "tab-pos");
      end;
   end Initialize;

   function Example_Application_Preferences_New
     (Window : not null access Example_Application_Window_Record'Class)
      return Example_Application_Preferences
   is
      Self : constant Example_Application_Preferences :=
        new Example_Application_Preferences_Record;
   begin
      Example.Application_Preferences.Initialize (Self, Window);
      return Self;
   end Example_Application_Preferences_New;

end Example.Application_Preferences;
