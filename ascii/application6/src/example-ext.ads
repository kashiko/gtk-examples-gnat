with Glib;        use Glib;
with Glib.Object; use Glib.Object;
with Gtk.Builder; use Gtk.Builder;
with Gtk.Dialog;  use Gtk.Dialog;

package Example.Ext is

   -- Builder

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtk_Builder;

   -- Dialog

   Ext_Use_Header_Bar : constant Gtk_Dialog_Flags := 2**2;

   -- Binding

   type Ext_Binding_Record is new GObject_Record with private;
   type Ext_Binding is access all Ext_Binding_Record'Class;

   type Ext_Binding_Flags is mod Integer'Last;
   Ext_Binding_Default        : constant Ext_Binding_Flags := 0;
   Ext_Binding_Bidirectional  : constant Ext_Binding_Flags := 1;
   Ext_Binding_Sync_Create    : constant Ext_Binding_Flags := 2;
   Ext_Binding_Invert_Boolean : constant Ext_Binding_Flags := 4;

   procedure Ext_Bind_Property (Source : not null access GObject_Record'Class;
      Source_Property : String; Target : not null access GObject_Record'Class;
      Target_Property                  : String;
      Flags : Ext_Binding_Flags := Ext_Binding_Default);
   function Ext_Bind_Property (Source : not null access GObject_Record'Class;
      Source_Property : String; Target : not null access GObject_Record'Class;
      Target_Property                 : String;
      Flags : Ext_Binding_Flags := Ext_Binding_Default) return Ext_Binding;

private

   -- Binding

   type Ext_Binding_Record is new GObject_Record with null record;

end Example.Ext;
