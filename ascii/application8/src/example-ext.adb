with Glib.Error; use Glib.Error;
with System;     use System;

package body Example.Ext is

   -- Builder

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtkada_Builder
   is
      Builder     : constant Gtkada_Builder := new Gtkada_Builder_Record;
      Error       : aliased GError;
      Return_Code : Guint;
   begin
      Gtkada.Builder.Initialize (Builder);
      Return_Code := Add_From_Resource (Builder, Resource_Path, Error'Access);
      if Error /= null then
         declare
            Message : constant String := Get_Message (Error);
         begin
            Error_Free (Error);
            raise Program_Error with Message;
         end;
      end if;
      return Builder;
   end Ext_Builder_New_From_Resource;

   -- Binding

   procedure Ext_Bind_Property (Source : not null access GObject_Record'Class;
      Source_Property : String; Target : not null access GObject_Record'Class;
      Target_Property                  : String;
      Flags : Ext_Binding_Flags := Ext_Binding_Default)
   is
      Result : Ext_Binding;
      pragma Unreferenced (Result);
   begin
      Result :=
        Ext_Bind_Property
          (Source, Source_Property, Target, Target_Property, Flags);
   end Ext_Bind_Property;

   function Ext_Bind_Property (Source : not null access GObject_Record'Class;
      Source_Property : String; Target : not null access GObject_Record'Class;
      Target_Property                 : String;
      Flags : Ext_Binding_Flags := Ext_Binding_Default) return Ext_Binding
   is
      function Internal (Source : System.Address; SP : String;
         Target                 : System.Address; TP : String;
         Flags                  : Ext_Binding_Flags) return System.Address;
      pragma Import (C, Internal, "g_object_bind_property");

      R      : System.Address;
      Result : Ext_Binding;
   begin
      R :=
        Internal
          (Source.Get_Object, Source_Property & ASCII.NUL, Target.Get_Object,
           Target_Property & ASCII.NUL, Flags);
      if R = System.Null_Address then
         return null;
      else
         Result := new Ext_Binding_Record;
         Result.Set_Object (R);
         return Result;
      end if;
   end Ext_Bind_Property;

end Example.Ext;
