with Ada.Directories; use Ada.Directories;
with GNAT.OS_Lib;     use GNAT.OS_Lib;

package body Example.Desktop is

   function Create_Config_Directory (Name : String) return String is
      Key_XDG_CONFIG_HOME   : constant String := "XDG_CONFIG_HOME";
      Value_XDG_CONFIG_HOME : String_Access   := Getenv (Key_XDG_CONFIG_HOME);
   begin
      if Value_XDG_CONFIG_HOME.all'Length > 0 then
         declare
            Path : constant String :=
              Compose (Value_XDG_CONFIG_HOME.all, Name);
         begin
            Free (Value_XDG_CONFIG_HOME);
            Create_Path (Path);
            return Path;
         end;
      end if;
      declare
         Key_HOME   : constant String := "HOME";
         Value_HOME : String_Access   := Getenv (Key_HOME);
      begin
         if Value_HOME.all'Length > 0 then
            declare
               Config_Path : constant String :=
                 Compose (Value_HOME.all, ".config");
               Path : constant String := Compose (Config_Path, Name);
            begin
               Free (Value_HOME);
               Create_Path (Path);
               return Path;
            end;
         end if;
      end;
      raise Constraint_Error with "Cannot find user home directory.";
   end Create_Config_Directory;

end Example.Desktop;
