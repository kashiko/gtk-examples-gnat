with Ada.Characters.Latin_1;   use Ada.Characters.Latin_1;
with Ada.Directories;          use Ada.Directories;
with Ada.Streams.Stream_IO;    use Ada.Streams.Stream_IO;
with Example.Desktop;          use Example.Desktop;
with Glib;                     use Glib;
with Glib.Properties;          use Glib.Properties;
with Glib.Properties.Creation; use Glib.Properties.Creation;
with Glib.Values;              use Glib.Values;
with GNATCOLL.Config;          use GNATCOLL.Config;
with System;

package body Example.Config is

   Klass : aliased Ada_GObject_Class := Uninitialized_Class;

   PROP_FONT            : constant Property_Id := 1;
   PROP_TRANSITION      : constant Property_Id := 2;
   PROP_TRANSITION_TYPE : constant Property_Id := 3;
   PROP_SHOW_WORDS      : constant Property_Id := 4;
   PROP_SHOW_LINES      : constant Property_Id := 5;

   function To_Transition_Type (S : String) return Gtk_Stack_Transition_Type is
   begin
      if S = "none" then
         return Stack_Transition_Type_None;
      elsif S = "crossfade" then
         return Stack_Transition_Type_Crossfade;
      elsif S = "slide-left-right" then
         return Stack_Transition_Type_Slide_Left_Right;
      else
         return Stack_Transition_Type_None;
      end if;
   end To_Transition_Type;

   procedure Prop_Set (Object : access GObject_Record'Class;
      Prop_Id : Property_Id; Value : GValue; Property_Spec : Param_Spec)
   is
   begin
      case Prop_Id is
         when PROP_FONT =>
            Example_Config (Object).Font :=
              To_Unbounded_String (Get_String (Value));
            Notify (Object, "font");
         when PROP_TRANSITION =>
            Example_Config (Object).Transition :=
              To_Unbounded_String (Get_String (Value));
            Notify (Object, "transition");
            Example_Config (Object).Transition_Type :=
              To_Transition_Type (Get_String (Value));
            Notify (Object, "transition-type");
         when PROP_TRANSITION_TYPE =>
            Example_Config (Object).Transition_Type :=
              Gtk_Stack_Transition_Type'Val (Get_Enum (Value));
            Notify (Object, "transition-type");
         when PROP_SHOW_WORDS =>
            Example_Config (Object).Show_Words := Get_Boolean (Value);
            Notify (Object, "show-words");
         when PROP_SHOW_LINES =>
            Example_Config (Object).Show_Lines := Get_Boolean (Value);
            Notify (Object, "show-lines");
         when others =>
            raise Program_Error
              with "Invalid property ID:" & Property_Id'Image (Prop_Id);
      end case;
   end Prop_Set;

   procedure Prop_Get (Object : access GObject_Record'Class;
      Prop_Id : Property_Id; Value : out GValue; Property_Spec : Param_Spec)
   is
   begin
      case Prop_Id is
         when PROP_FONT =>
            Init (Value, GType_String);
            Set_String (Value, To_String (Example_Config (Object).Font));
         when PROP_TRANSITION =>
            Init (Value, GType_String);
            Set_String (Value, To_String (Example_Config (Object).Transition));
         when PROP_TRANSITION_TYPE =>
            Init (Value, GType_Int);
            Set_Int
              (Value,
               Gtk_Stack_Transition_Type'Pos
                 (Example_Config (Object).Transition_Type));
         when PROP_SHOW_WORDS =>
            Init (Value, GType_Boolean);
            Set_Boolean (Value, Example_Config (Object).Show_Words);
         when PROP_SHOW_LINES =>
            Init (Value, GType_Boolean);
            Set_Boolean (Value, Example_Config (Object).Show_Lines);
         when others =>
            raise Program_Error
              with "Invalid property ID:" & Property_Id'Image (Prop_Id);
      end case;
   end Prop_Get;

   procedure Class_Init (Self : GObject_Class);
   pragma Convention (C, Class_Init);

   procedure Class_Init (Self : GObject_Class) is
   begin
      Set_Properties_Handlers (Self, Prop_Set'Access, Prop_Get'Access);

      Install_Property
        (Self, PROP_FONT,
         Gnew_String
           ("font", "Font", "The font to be used for content.",
            "Monospace 12"));
      Install_Property
        (Self, PROP_TRANSITION,
         Gnew_String
           ("transition", "Transition",
            "The transition to use when switching tabs.", "none"));
      Install_Property
        (Self, PROP_TRANSITION_TYPE,
         Gnew_Int
           ("transition-type", "Transition type",
            "The transition to use when switching tabs.",
            Gtk_Stack_Transition_Type'Pos (Stack_Transition_Type_None),
            Gtk_Stack_Transition_Type'Pos
              (Stack_Transition_Type_Over_Right_Left),
            Gtk_Stack_Transition_Type'Pos (Stack_Transition_Type_None)));
      Install_Property
        (Self, PROP_SHOW_WORDS,
         Gnew_Boolean
           ("show-words", "Show words",
            "Whether to show a word list in the sidebar.", False));
      Install_Property
        (Self, PROP_SHOW_LINES,
         Gnew_Boolean
           ("show-lines", "Show lines",
            "Whether to show the count of lines in the header bar.", False));
   end Class_Init;

   function Get_Type return GType is
   begin
      if Initialize_Class_Record
          (Ancestor  => GType_Object, Class_Record => Klass'Access,
           Type_Name => "Example_Config", Class_Init => Class_Init'Access)
      then
         null;
      end if;
      return Klass.The_Type;
   end Get_Type;

   procedure Initialize (Self : access Example_Config_Record'Class) is
   begin
      G_New (Self, Get_Type);

      Self.Config_Directory :=
        To_Unbounded_String
          (Create_Config_Directory ("gtk-examples-gnat-exampleapp"));
   end Initialize;

   function Example_Config_New return Example_Config is
      Self : constant Example_Config := new Example_Config_Record;
   begin
      Example.Config.Initialize (Self);
      return Self;
   end Example_Config_New;

   function Get_Config_Path (Config : Example_Config) return String is
   begin
      return Compose (To_String (Config.Config_Directory), "config");
   end Get_Config_Path;

   procedure Read (Config : in out Example_Config) is

      procedure Set (Variable : in out Unbounded_String; Pool : Config_Pool;
         Key                  :        String; Section : String)
      is
         S : String := Get (Pool, Key, Section);
      begin
         if S'Length > 0 then
            Variable := To_Unbounded_String (S);
         end if;
      end Set;

      procedure Set (Variable : in out Boolean; Pool : Config_Pool;
         Key                  :        String; Section : String)
      is
         S : String := Get (Pool, Key, Section);
      begin
         if S'Length > 0 then
            Variable := Get_Boolean (Pool, Key, Section);
         end if;
      end Set;

   begin
      if Exists (Get_Config_Path (Config)) then
         declare
            Pool   : Config_Pool;
            Parser : INI_Parser;
         begin
            Open (Parser, Get_Config_Path (Config));
            Fill (Pool, Parser);
            Set (Config.Font, Pool, "font", "main");
            Set (Config.Transition, Pool, "transition", "main");
            Config.Transition_Type :=
              To_Transition_Type (To_String (Config.Transition));
            Set (Config.Show_Words, Pool, "show-words", "main");
            Set (Config.Show_Lines, Pool, "show-lines", "main");
         end;
      end if;
   end Read;

   procedure Write (Config : Example_Config) is
      Buffer : Unbounded_String;
      File   : Ada.Streams.Stream_IO.File_Type;
      S      : Stream_Access;

      function To_String (Value : Boolean) return String is
      begin
         if Value then
            return "true";
         else
            return "false";
         end if;
      end To_String;

   begin
      Append (Buffer, "[main]" & LF);
      Append (Buffer, "font = " & Config.Font & LF);
      Append (Buffer, "transition = " & Config.Transition & LF);
      Append (Buffer, "show-words = " & To_String (Config.Show_Words) & LF);
      Append (Buffer, "show-lines = " & To_String (Config.Show_Lines) & LF);

      Create (File, Out_File, Get_Config_Path (Config));
      S := Stream (File);
      String'Write (S, To_String (Buffer));
      Close (File);
   end Write;

end Example.Config;
