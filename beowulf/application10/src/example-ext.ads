with Glib;           use Glib;
with Gtkada.Builder; use Gtkada.Builder;

package Example.Ext is

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtkada_Builder;

end Example.Ext;
