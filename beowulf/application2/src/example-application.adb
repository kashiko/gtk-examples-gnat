with Ada.Text_IO;
with Example.Application_Window; use Example.Application_Window;
with Gtk.Widget;                 use Gtk.Widget;

package body Example.Application is

   procedure Activate_Handler (Self : access Gapplication_Record'Class) is
      Window : Example_Application_Window :=
        Example_Application_Window_New (Example_Application (Self));
   begin
      Present (Window);
   end Activate_Handler;

   procedure Open_Handler (Application : Gtkada_Application;
      Files                            : GFile_Array)
   is
      use Widget_List;
      Windows : Glist;
      Window  : Example_Application_Window;
   begin
      Windows := Get_Windows (Application);
      if Length (Windows) > 0 then
         Window := Example_Application_Window (Get_Data (Windows));
      else
         Window :=
           Example_Application_Window_New (Example_Application (Application));
      end if;

      for File of Files loop
         Open (Window, File);
      end loop;

      Present (Window);
   end Open_Handler;

   procedure Initialize
     (Self           : not null access Example_Application_Record'Class;
      Application_Id : UTF8_String := ""; Flags : GApplication_Flags;
      Gtkada_Flags   : Gtkada_Application_Flags)
   is
   begin
      Gtkada.Application.Initialize
        (Self, Application_Id, Flags, Gtkada_Flags);

      On_Activate (Self, Activate_Handler'Access);
      On_Open (Self, Open_Handler'Access);
   end Initialize;

   function Example_Application_New (Application_Id : UTF8_String := "";
      Flags                                         : GApplication_Flags;
      Gtkada_Flags : Gtkada_Application_Flags) return Example_Application
   is
      Self : constant Example_Application := new Example_Application_Record;
   begin
      Example.Application.Initialize
        (Self, Application_Id, Flags, Gtkada_Flags);
      return Self;
   end Example_Application_New;

end Example.Application;
