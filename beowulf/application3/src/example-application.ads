with Glib;               use Glib;
with Glib.Application;   use Glib.Application;
with Gtkada.Application; use Gtkada.Application;

package Example.Application is

   type Example_Application_Record is new Gtkada_Application_Record with
   null record;
   type Example_Application is access all Example_Application_Record'Class;

   procedure Initialize
     (Self           : not null access Example_Application_Record'Class;
      Application_Id : UTF8_String := ""; Flags : GApplication_Flags;
      Gtkada_Flags   : Gtkada_Application_Flags);
   function Example_Application_New (Application_Id : UTF8_String := "";
      Flags                                         : GApplication_Flags;
      Gtkada_Flags : Gtkada_Application_Flags) return Example_Application;

end Example.Application;
