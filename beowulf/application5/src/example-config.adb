with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Directories;        use Ada.Directories;
with Ada.Streams.Stream_IO;  use Ada.Streams.Stream_IO;
with Example.Desktop;        use Example.Desktop;
with GNATCOLL.Config;        use GNATCOLL.Config;

package body Example.Config is

   procedure Initialize (Config : in out Example_Config) is
   begin
      Config.Config_Directory :=
        To_Unbounded_String
          (Create_Config_Directory ("gtk-examples-gnat-exampleapp"));
   end Initialize;

   function Get_Config_Path (Config : Example_Config) return String is
   begin
      return Compose (To_String (Config.Config_Directory), "config");
   end Get_Config_Path;

   procedure Read (Config : in out Example_Config) is

      procedure Set (Variable : in out Unbounded_String; Pool : Config_Pool;
         Key                  :        String; Section : String)
      is
         S : String := Get (Pool, Key, Section);
      begin
         if S'Length > 0 then
            Variable := To_Unbounded_String (S);
         end if;
      end Set;

   begin
      if Exists (Get_Config_Path (Config)) then
         declare
            Pool   : Config_Pool;
            Parser : INI_Parser;
         begin
            Open (Parser, Get_Config_Path (Config));
            Fill (Pool, Parser);
            Set (Config.Font, Pool, "font", "main");
            Set (Config.Transition, Pool, "transition", "main");
         end;
      end if;
   end Read;

   procedure Write (Config : Example_Config) is
      Buffer : Unbounded_String;
      File   : Ada.Streams.Stream_IO.File_Type;
      S      : Stream_Access;
   begin
      Append (Buffer, "[main]" & LF);
      Append (Buffer, "font = " & Config.Font & LF);
      Append (Buffer, "transition = " & Config.Transition & LF);

      Create (File, Out_File, Get_Config_Path (Config));
      S := Stream (File);
      String'Write (S, To_String (Buffer));
      Close (File);
   end Write;

end Example.Config;
