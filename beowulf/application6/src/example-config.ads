with Glib.Object; use Glib.Object;

private with Ada.Strings.Unbounded;
private with Gtk.Stack;

package Example.Config is

   type Example_Config_Record is new GObject_Record with private;
   type Example_Config is access all Example_Config_Record;

   procedure Initialize (Self : access Example_Config_Record'Class);
   function Example_Config_New return Example_Config;

   procedure Read (Config : in out Example_Config);
   procedure Write (Config : Example_Config);

private

   use Ada.Strings.Unbounded;
   use Gtk.Stack;

   type Example_Config_Record is new GObject_Record with record
      Config_Directory : Unbounded_String;

      Font : Unbounded_String          := To_Unbounded_String ("Monospace 12");
      Transition      : Unbounded_String := To_Unbounded_String ("none");
      Transition_Type : Gtk_Stack_Transition_Type :=
        Stack_Transition_Type_None;
   end record;

end Example.Config;
