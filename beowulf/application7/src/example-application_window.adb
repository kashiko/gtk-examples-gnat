with Ada.Directories;       use Ada.Directories;
with Ada.IO_Exceptions;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Example.Config;        use Example.Config;
with Example.Ext;           use Example.Ext;
with Example.Streams;       use Example.Streams;
with Glib;                  use Glib;
with Glib.Application;      use Glib.Application;
with Glib.Object;           use Glib.Object;
with Gtk.Scrolled_Window;   use Gtk.Scrolled_Window;
with Gtk.Search_Entry;      use Gtk.Search_Entry;
with Gtk.Text_Buffer;       use Gtk.Text_Buffer;
with Gtk.Text_Iter;         use Gtk.Text_Iter;
with Gtk.Text_Tag;          use Gtk.Text_Tag;
with Gtk.Text_View;         use Gtk.Text_View;
with Gtk.Widget;            use Gtk.Widget;
with Gtkada.Builder;        use Gtkada.Builder;

package body Example.Application_Window is

   procedure Search_Text_Changed (Builder : access Gtkada_Builder_Record'Class)
   is
      Search_Entry : constant Gtk_Search_Entry :=
        Gtk_Search_Entry (Get_Object (Builder, "searchentry"));
      Text : String_Access := new String'(Get_Text (Search_Entry));
   begin
      if Text'Length < 1 then
         Free (Text);
         return;
      end if;

      declare
         Window : constant Example_Application_Window :=
           Example_Application_Window
             (Get_Toplevel (Gtk_Widget (Search_Entry)));
         Tab : constant Gtk_Scrolled_Window :=
           Gtk_Scrolled_Window (Get_Visible_Child (Window.Stack));
         View : constant Gtk_Text_View   := Gtk_Text_View (Get_Child (Tab));
         Buffer : constant Gtk_Text_Buffer := Get_Buffer (View);
         Start, Match_Start, Match_End : Gtk_Text_Iter;
         Search_Result                 : Boolean;
      begin
         -- Very simple-minded search implementation
         Get_Start_Iter (Buffer, Start);
         Forward_Search
           (Start, Text.all, Gtk.Text_Iter.Case_Insensitive, Match_Start,
            Match_End, Null_Text_Iter, Search_Result);
         if Search_Result then
            Select_Range (Buffer, Match_Start, Match_End);
            declare
               Scroll_Result : constant Boolean :=
                 Scroll_To_Iter (View, Match_Start, 0.0, False, 0.0, 0.0);
            begin
               null;
            end;
         end if;
      end;
      Free (Text);
   end Search_Text_Changed;

   procedure Visible_Child_Changed
     (Builder : access Gtkada_Builder_Record'Class)
   is
   begin
      if In_Destruction (Gtk_Widget (Get_Object (Builder, "stack"))) then
         return;
      end if;

      declare
         Window : constant Example_Application_Window :=
           Example_Application_Window
             (Get_Toplevel (Gtk_Widget (Get_Object (Builder, "stack"))));
      begin
         Set_Search_Mode (Window.Search_Bar, False);
      end;
   end Visible_Child_Changed;

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class)
   is
   begin
      Gtk.Application_Window.Initialize (Self, Application);

      declare
         Builder : constant Gtkada_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/window.ui");
         Window : constant Gtk_Application_Window :=
           Gtk_Application_Window (Get_Object (Builder, "main_window"));
         Content : constant Gtk_Widget :=
           Gtk_Widget (Get_Object (Builder, "content_box"));
         Width, Height : Gint;
      begin
         Set_Title (Self, Get_Title (Window));
         Get_Default_Size (Window, Width, Height);
         Set_Default_Size (Self, Width, Height);

         Ref (Content);
         Remove (Window, Content);
         Add (Self, Content);
         Unref (Content);

         Self.Stack      := Gtk_Stack (Get_Object (Builder, "stack"));
         Self.Search     := Gtk_Toggle_Button (Get_Object (Builder, "search"));
         Self.Search_Bar := Gtk_Search_Bar (Get_Object (Builder, "searchbar"));

         Register_Handler
           (Builder, "search_text_changed", Search_Text_Changed'Access);
         Register_Handler
           (Builder, "visible_child_changed", Visible_Child_Changed'Access);
         Do_Connect (Builder);

         Destroy (Window);
      end;

      Bind_Property
        (Application.Config, "transition-type", Self.Stack, "transition-type");
      Notify (Application.Config, "transition-type");

      Bind_Property
        (Self.Search, "active", Self.Search_Bar, "search-mode-enabled",
         Binding_Bidirectional);
   end Initialize;

   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window
   is
      Self : constant Example_Application_Window :=
        new Example_Application_Window_Record;
   begin
      Example.Application_Window.Initialize (Self, Application);
      return Self;
   end Example_Application_Window_New;

   Invalid_Path_Error : exception;

   function Read_All (Path : String) return Unbounded_String is
   begin
      if Path'Length < 1 or else not Exists (Path)
        or else Kind (Path) /= Ordinary_File then
         raise Invalid_Path_Error;
      end if;
      return R : Unbounded_String do
         declare
            File : Ada.Streams.Stream_IO.File_Type;
            S    : Stream_Access;
         begin
            Open (File, In_File, Path);
            S := Stream (File);
            declare
               Buffer_Size  : constant := 512;
               Input_Stream : aliased Buffered_Stream_Type (S, Buffer_Size);
            begin
               loop
                  declare
                     C : Character;
                  begin
                     Character'Read (Input_Stream'Access, C);
                     Append (R, C);
                  end;
               end loop;
            exception
               when Ada.IO_Exceptions.End_Error =>
                  null;
            end;
            Close (File);
         end;
      end return;
   end Read_All;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile)
   is
      Path     : constant UTF8_String         := Get_Path (File);
      Basename : constant UTF8_String         := Simple_Name (Path);
      Scrolled : constant Gtk_Scrolled_Window := Gtk_Scrolled_Window_New;
      View     : constant Gtk_Text_View       := Gtk_Text_View_New;
      Buffer   : constant Gtk_Text_Buffer     := Get_Buffer (View);
   begin
      Show (Scrolled);
      Set_Hexpand (Scrolled, True);
      Set_Vexpand (Scrolled, True);
      Set_Editable (View, False);
      Set_Cursor_Visible (View, False);
      Show (View);
      Add (Scrolled, View);
      Add_Titled (Self.Stack, Scrolled, Basename, Basename);

      declare
         Text : Unbounded_String;
      begin
         Text := Read_All (Path);
         Set_Text (Buffer, To_String (Text));
      exception
         when Invalid_Path_Error =>
            null;
      end;

      declare
         Tag                  : constant Gtk_Text_Tag := Create_Tag (Buffer);
         Start_Iter, End_Iter : Gtk_Text_Iter;
      begin
         Bind_Property
           (Example_Application (Get_Default).Config, "font", Tag, "font");
         Notify (Example_Application (Get_Default).Config, "font");

         Get_Start_Iter (Buffer, Start_Iter);
         Get_End_Iter (Buffer, End_Iter);
         Apply_Tag (Buffer, Tag, Start_Iter, End_Iter);
      end;

      Set_Sensitive (Self.Search, True);
   end Open;

end Example.Application_Window;
