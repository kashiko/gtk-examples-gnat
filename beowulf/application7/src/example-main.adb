with Example.Application; use Example.Application;
with Glib;                use Glib;
with Glib.Application;    use Glib.Application;
with GNAT.OS_Lib;         use GNAT.OS_Lib;
with Gtkada.Application;  use Gtkada.Application;

procedure Example.Main is
   Application : Example_Application;
   Result      : Gint;
begin
   Application :=
     Example_Application_New
       ("com.gitlab.kashiko.gtk-examples-gnat.exampleapp",
        G_Application_Handles_Open, Gtkada_Application_Handles_Open);
   Result := Run (Application);
   OS_Exit (Integer (Result));
end Example.Main;
