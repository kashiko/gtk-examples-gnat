with Example.Application;    use Example.Application;
with Gtk.Application_Window; use Gtk.Application_Window;
with Gtkada.Application;     use Gtkada.Application;

private with Gtk.List_Box;
private with Gtk.Menu_Button;
private with Gtk.Revealer;
private with Gtk.Search_Bar;
private with Gtk.Search_Entry;
private with Gtk.Stack;
private with Gtk.Toggle_Button;

package Example.Application_Window is

   type Example_Application_Window_Record is
     new Gtk_Application_Window_Record with private;
   type Example_Application_Window is
     access all Example_Application_Window_Record;

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class);
   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile);

private

   use Gtk.List_Box;
   use Gtk.Menu_Button;
   use Gtk.Revealer;
   use Gtk.Search_Bar;
   use Gtk.Search_Entry;
   use Gtk.Stack;
   use Gtk.Toggle_Button;

   type Example_Application_Window_Record is new Gtk_Application_Window_Record with
   record
      Stack        : Gtk_Stack;
      Search       : Gtk_Toggle_Button;
      Search_Bar   : Gtk_Search_Bar;
      Search_Entry : Gtk_Search_Entry;
      Gears        : Gtk_Menu_Button;
      Sidebar      : Gtk_Revealer;
      Words        : Gtk_List_Box;
   end record;

end Example.Application_Window;
