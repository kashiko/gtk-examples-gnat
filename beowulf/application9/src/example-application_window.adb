with Ada.Containers.Hashed_Sets;
with Ada.Directories;       use Ada.Directories;
with Ada.IO_Exceptions;
with Ada.Streams.Stream_IO; use Ada.Streams.Stream_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Hash;
with Example.Config;        use Example.Config;
with Example.Ext;           use Example.Ext;
with Example.Streams;       use Example.Streams;
with Glib;                  use Glib;
with Glib.Application;      use Glib.Application;
with Glib.Object;           use Glib.Object;
with Glib.Unicode;          use Glib.Unicode;
with Gtk.Button;            use Gtk.Button;
with Gtk.Popover;           use Gtk.Popover;
with Gtk.Scrolled_Window;   use Gtk.Scrolled_Window;
with Gtk.Text_Buffer;       use Gtk.Text_Buffer;
with Gtk.Text_Iter;         use Gtk.Text_Iter;
with Gtk.Text_Tag;          use Gtk.Text_Tag;
with Gtk.Text_View;         use Gtk.Text_View;
with Gtk.Widget;            use Gtk.Widget;
with Gtkada.Builder;        use Gtkada.Builder;

package body Example.Application_Window is

   procedure Search_Text_Changed (Builder : access Gtkada_Builder_Record'Class)
   is
      Search_Entry : constant Gtk_Search_Entry :=
        Gtk_Search_Entry (Get_Object (Builder, "searchentry"));
      Text : String_Access := new String'(Get_Text (Search_Entry));
   begin
      if Text'Length < 1 then
         Free (Text);
         return;
      end if;

      declare
         Window : constant Example_Application_Window :=
           Example_Application_Window
             (Get_Toplevel (Gtk_Widget (Search_Entry)));
         Tab : constant Gtk_Scrolled_Window :=
           Gtk_Scrolled_Window (Get_Visible_Child (Window.Stack));
         View : constant Gtk_Text_View   := Gtk_Text_View (Get_Child (Tab));
         Buffer : constant Gtk_Text_Buffer := Get_Buffer (View);
         Start, Match_Start, Match_End : Gtk_Text_Iter;
         Search_Result                 : Boolean;
      begin
         -- Very simple-minded search implementation
         Get_Start_Iter (Buffer, Start);
         Forward_Search
           (Start, Text.all, Gtk.Text_Iter.Case_Insensitive, Match_Start,
            Match_End, Null_Text_Iter, Search_Result);
         if Search_Result then
            Select_Range (Buffer, Match_Start, Match_End);
            declare
               Scroll_Result : constant Boolean :=
                 Scroll_To_Iter (View, Match_Start, 0.0, False, 0.0, 0.0);
            begin
               null;
            end;
         end if;
      end;
      Free (Text);
   end Search_Text_Changed;

   procedure Find_Word (Self : access Gtk_Button_Record'Class) is
      Window : constant Example_Application_Window :=
        Example_Application_Window (Get_Toplevel (Self));
      Word : constant String := Get_Label (Self);
   begin
      Set_Text (Window.Search_Entry, Word);
   end Find_Word;

   procedure Update_Words
     (Window : not null access Example_Application_Window_Record'Class)
   is
      Tab : constant Gtk_Scrolled_Window :=
        Gtk_Scrolled_Window (Get_Visible_Child (Window.Stack));
   begin
      if Tab = null then
         return;
      end if;

      declare
         View   : constant Gtk_Text_View   := Gtk_Text_View (Get_Child (Tab));
         Buffer : constant Gtk_Text_Buffer := Get_Buffer (View);

         package String_Sets is new Ada.Containers.Hashed_Sets
           (Unbounded_String, Ada.Strings.Unbounded.Hash, "=");
         use String_Sets;
         Strings : Set;

         Start_Iter, End_Iter : Gtk_Text_Iter;
         Result               : Boolean;
      begin
         Get_Start_Iter (Buffer, Start_Iter);
         while not Is_End (Start_Iter) loop
            while not Starts_Word (Start_Iter) loop
               Forward_Char (Start_Iter, Result);
               if not Result then
                  goto Done;
               end if;
            end loop;
            End_Iter := Start_Iter;
            Forward_Word_End (End_Iter, Result);
            if not Result then
               goto Done;
            end if;
            declare
               Word : constant String :=
                 Get_Text (Buffer, Start_Iter, End_Iter);
            begin
               Include (Strings, To_Unbounded_String (UTF8_Strdown (Word)));
            end;
            Start_Iter := End_Iter;
         end loop;
         <<Done>>
         declare
            use Widget_List;
            Children : Glist := Get_Children (Window.Words);
            L        : Glist := Children;
         begin
            while L /= Null_List loop
               Remove (Window.Words, Get_Data (L));
               L := Next (L);
            end loop;
            Free (Children);

            for S of Strings loop
               declare
                  Row : constant Gtk_Button :=
                    Gtk_Button_New_With_Label (To_String (S));
               begin
                  On_Clicked (Row, Find_Word'Access);
                  Show (Row);
                  Add (Window.Words, Row);
               end;
            end loop;
         end;
      end;
   end Update_Words;

   procedure Update_Lines
     (Window : not null access Example_Application_Window_Record'Class)
   is
      Tab : constant Gtk_Scrolled_Window :=
        Gtk_Scrolled_Window (Get_Visible_Child (Window.Stack));
   begin
      if Tab = null then
         return;
      end if;

      declare
         View   : constant Gtk_Text_View   := Gtk_Text_View (Get_Child (Tab));
         Buffer : constant Gtk_Text_Buffer := Get_Buffer (View);
         Count  : Natural                  := 0;
         Iter   : Gtk_Text_Iter;
         Result : Boolean;
      begin
         Get_Start_Iter (Buffer, Iter);
         while not Is_End (Iter) loop
            Count := Count + 1;
            Forward_Line (Iter, Result);
            exit when not Result;
         end loop;

         declare
            Lines : constant String := Natural'Image (Count);
         begin
            Set_Text (Window.Lines, Lines (Lines'First + 1 .. Lines'Last));
         end;
      end;
   end Update_Lines;

   procedure Visible_Child_Changed
     (Builder : access Gtkada_Builder_Record'Class)
   is
   begin
      if In_Destruction (Gtk_Widget (Get_Object (Builder, "stack"))) then
         return;
      end if;

      declare
         Window : constant Example_Application_Window :=
           Example_Application_Window
             (Get_Toplevel (Gtk_Widget (Get_Object (Builder, "stack"))));
      begin
         Set_Search_Mode (Window.Search_Bar, False);
         Update_Words (Window);
         Update_Lines (Window);
      end;
   end Visible_Child_Changed;

   procedure Words_Changed (Builder : access Gtkada_Builder_Record'Class) is
      Window : constant Example_Application_Window :=
        Example_Application_Window
          (Get_Toplevel (Gtk_Widget (Get_Object (Builder, "sidebar"))));
   begin
      Update_Words (Window);
   end Words_Changed;

   procedure Initialize
     (Self        : not null access Example_Application_Window_Record'Class;
      Application : not null access Example_Application_Record'Class)
   is
   begin
      Gtk.Application_Window.Initialize (Self, Application);

      declare
         Builder : constant Gtkada_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/window.ui");
         Window : constant Gtk_Application_Window :=
           Gtk_Application_Window (Get_Object (Builder, "main_window"));
         Content : constant Gtk_Widget :=
           Gtk_Widget (Get_Object (Builder, "content_box"));
         Width, Height : Gint;
      begin
         Set_Title (Self, Get_Title (Window));
         Get_Default_Size (Window, Width, Height);
         Set_Default_Size (Self, Width, Height);

         Ref (Content);
         Remove (Window, Content);
         Add (Self, Content);
         Unref (Content);

         Self.Stack        := Gtk_Stack (Get_Object (Builder, "stack"));
         Self.Search := Gtk_Toggle_Button (Get_Object (Builder, "search"));
         Self.Search_Bar := Gtk_Search_Bar (Get_Object (Builder, "searchbar"));
         Self.Search_Entry :=
           Gtk_Search_Entry (Get_Object (Builder, "searchentry"));
         Self.Gears       := Gtk_Menu_Button (Get_Object (Builder, "gears"));
         Self.Sidebar     := Gtk_Revealer (Get_Object (Builder, "sidebar"));
         Self.Words       := Gtk_List_Box (Get_Object (Builder, "words"));
         Self.Lines       := Gtk_Label (Get_Object (Builder, "lines"));
         Self.Lines_Label := Gtk_Label (Get_Object (Builder, "lines_label"));

         Register_Handler
           (Builder, "search_text_changed", Search_Text_Changed'Access);
         Register_Handler
           (Builder, "visible_child_changed", Visible_Child_Changed'Access);
         Register_Handler (Builder, "words_changed", Words_Changed'Access);
         Do_Connect (Builder);

         Destroy (Window);
      end;

      Bind_Property
        (Application.Config, "transition-type", Self.Stack, "transition-type");
      Notify (Application.Config, "transition-type");

      Bind_Property
        (Application.Config, "show-words", Self.Sidebar, "reveal-child");
      Notify (Application.Config, "show-words");

      Bind_Property
        (Self.Search, "active", Self.Search_Bar, "search-mode-enabled",
         Binding_Bidirectional);

      declare
         Builder : constant Gtkada_Builder :=
           Ext_Builder_New_From_Resource
             ("/com/gitlab/kashiko/gtk-examples-gnat/exampleapp/gears-menu.ui");
      begin
         Set_Popover
           (Self.Gears, Gtk_Popover (Get_Object (Builder, "popover")));
         Bind_Property
           (Application.Config, "show-words",
            Get_Object (Builder, "words_item"), "active",
            Binding_Bidirectional);
         Notify (Application.Config, "show-words");
         Bind_Property
           (Application.Config, "show-lines",
            Get_Object (Builder, "lines_item"), "active",
            Binding_Bidirectional);
         Notify (Application.Config, "show-lines");
         Unref (Builder);
      end;

      Bind_Property (Application.Config, "show-lines", Self.Lines, "visible");
      Bind_Property (Self.Lines, "visible", Self.Lines_Label, "visible");
      Notify (Application.Config, "show-lines");
   end Initialize;

   function Example_Application_Window_New
     (Application : not null access Example_Application_Record'Class)
      return Example_Application_Window
   is
      Self : constant Example_Application_Window :=
        new Example_Application_Window_Record;
   begin
      Example.Application_Window.Initialize (Self, Application);
      return Self;
   end Example_Application_Window_New;

   Invalid_Path_Error : exception;

   function Read_All (Path : String) return Unbounded_String is
   begin
      if Path'Length < 1 or else not Exists (Path)
        or else Kind (Path) /= Ordinary_File then
         raise Invalid_Path_Error;
      end if;
      return R : Unbounded_String do
         declare
            File : Ada.Streams.Stream_IO.File_Type;
            S    : Stream_Access;
         begin
            Open (File, In_File, Path);
            S := Stream (File);
            declare
               Buffer_Size  : constant := 512;
               Input_Stream : aliased Buffered_Stream_Type (S, Buffer_Size);
            begin
               loop
                  declare
                     C : Character;
                  begin
                     Character'Read (Input_Stream'Access, C);
                     Append (R, C);
                  end;
               end loop;
            exception
               when Ada.IO_Exceptions.End_Error =>
                  null;
            end;
            Close (File);
         end;
      end return;
   end Read_All;

   procedure Open
     (Self : not null access Example_Application_Window_Record'Class;
      File : GFile)
   is
      Path     : constant UTF8_String         := Get_Path (File);
      Basename : constant UTF8_String         := Simple_Name (Path);
      Scrolled : constant Gtk_Scrolled_Window := Gtk_Scrolled_Window_New;
      View     : constant Gtk_Text_View       := Gtk_Text_View_New;
      Buffer   : constant Gtk_Text_Buffer     := Get_Buffer (View);
   begin
      Show (Scrolled);
      Set_Hexpand (Scrolled, True);
      Set_Vexpand (Scrolled, True);
      Set_Editable (View, False);
      Set_Cursor_Visible (View, False);
      Show (View);
      Add (Scrolled, View);
      Add_Titled (Self.Stack, Scrolled, Basename, Basename);

      declare
         Text : Unbounded_String;
      begin
         Text := Read_All (Path);
         Set_Text (Buffer, To_String (Text));
      exception
         when Invalid_Path_Error =>
            null;
      end;

      declare
         Tag                  : constant Gtk_Text_Tag := Create_Tag (Buffer);
         Start_Iter, End_Iter : Gtk_Text_Iter;
      begin
         Bind_Property
           (Example_Application (Get_Default).Config, "font", Tag, "font");
         Notify (Example_Application (Get_Default).Config, "font");

         Get_Start_Iter (Buffer, Start_Iter);
         Get_End_Iter (Buffer, End_Iter);
         Apply_Tag (Buffer, Tag, Start_Iter, End_Iter);
      end;

      Set_Sensitive (Self.Search, True);

      Update_Words (Self);
      Update_Lines (Self);
   end Open;

end Example.Application_Window;
