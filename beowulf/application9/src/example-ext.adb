with Glib.Error; use Glib.Error;

package body Example.Ext is

   function Ext_Builder_New_From_Resource
     (Resource_Path : UTF8_String) return Gtkada_Builder
   is
      Builder     : constant Gtkada_Builder := new Gtkada_Builder_Record;
      Error       : aliased GError;
      Return_Code : Guint;
   begin
      Initialize (Builder);
      Return_Code := Add_From_Resource (Builder, Resource_Path, Error'Access);
      if Error /= null then
         declare
            Message : constant String := Get_Message (Error);
         begin
            Error_Free (Error);
            raise Program_Error with Message;
         end;
      end if;
      return Builder;
   end Ext_Builder_New_From_Resource;

end Example.Ext;
