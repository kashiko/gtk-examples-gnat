package body Example.Streams is

   procedure Read (Stream : in out Buffered_Stream_Type;
      Item :    out Stream_Element_Array; Last : out Stream_Element_Offset)
   is
   begin
      if Stream.Index > Stream.Last then
         Read (Stream.Stream.all, Stream.Buffer, Stream.Last);
         Stream.Index := 1;
      end if;
      declare
         Count : constant Stream_Element_Count :=
           Stream_Element_Count'Min (Stream.Last - Stream.Index + 1,
              Item'Length);
      begin
         Item (Item'First .. Item'First + Count - 1) :=
           Stream.Buffer (Stream.Index .. Stream.Index + Count - 1);
         Last         := Item'First + Count - 1;
         Stream.Index := Stream.Index + Count;
      end;
   end Read;

   procedure Unget (Stream : in out Buffered_Stream_Type) is
   begin
      Stream.Index := Stream.Index - 1;
   end Unget;

   procedure Write (Stream : in out Buffered_Stream_Type;
      Item                 :        Stream_Element_Array)
   is
   begin
      if Stream.Last + Item'Length <= Stream.Buffer_Size then
         Stream.Buffer (Stream.Last + 1 .. Stream.Last + Item'Length) := Item;
         Stream.Last := Stream.Last + Item'Length;
      elsif Stream.Last > 0 then
         Flush (Stream);
         Write (Stream, Item);
      else
         Write (Stream.Stream.all, Item);
      end if;
   end Write;

   procedure Flush (Stream : in out Buffered_Stream_Type) is
   begin
      Write (Stream.Stream.all, Stream.Buffer (1 .. Stream.Last));
      Stream.Last := 0;
   end Flush;

end Example.Streams;
